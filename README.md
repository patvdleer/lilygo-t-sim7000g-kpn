# LilyGo SIM7000G

Report GPS position via WiFi to MQTT or via KPN m2m flow

Bought @ https://www.tinytronics.nl/shop/en/development-boards/microcontroller-boards/with-wi-fi/lilygo-ttgo-t-sim7000g-esp32-wrover-with-18650-battery-holder

## init

Copy `src/secrets.h.example` to `src/secrets.h` and update the details before compiling

See https://github.com/Xinyuan-LilyGO/LilyGO-T-SIM7000G for more info

Tested / dev on LilyGO T-SIM7000G 20200415

## PinOut 20200415

![](https://raw.githubusercontent.com/Xinyuan-LilyGO/LilyGO-T-SIM7000G/master/Historical/SIM7000G_20200415/pins.jpg)
