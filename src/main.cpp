#include <Arduino.h>
#include <WiFi.h>
#include "driver/adc.h"
#include <SPI.h>
#include <SD.h>

#include "secrets.h"

// set a higher precision than 4 for lat/long
#define SENML_MAX_DOUBLE_PRECISION 8

#define LOG_TO_SD true
#define LOG_FILE "/log"

#define WEBSERIAL true

#include <thingsml_http.h>

#define TINY_GSM_MODEM_SIM7000
#define TINY_GSM_RX_BUFFER 1024
#define SerialAT Serial1
#define SerialMon Serial

unsigned long intervalGpsViaGsmMillis = 60*1000UL;
unsigned long intervalGpsViaWifiMillis = 60*1000UL;
unsigned long intervalWifiMillis = 60*1000UL;

// See all AT commands, if wanted
// #define DUMP_AT_COMMANDS

// for debugging purposes
// #define NO_WIFI_LOGGING

#define UART_BAUD   9600
#define PIN_DTR     25
#define PIN_TX      27
#define PIN_RX      26
#define PWR_PIN     4

#define SD_MISO     2
#define SD_MOSI     15
#define SD_SCLK     14
#define SD_CS       13
#define LED_PIN     12

#define BAT_ADC_PIN 35
#define SOL_ADC_PIN 36
#define ADC_SAMPLES 5

#define uS_TO_S_FACTOR 1000000
#define mS_TO_S_FACTOR 1000

#include <TinyGsmClient.h>

#ifdef DUMP_AT_COMMANDS
  #include <StreamDebugger.h>
  StreamDebugger debugger(SerialAT, SerialMon);
  TinyGsm modem(debugger);
#else
  TinyGsm modem(SerialAT);
#endif

TinyGsmClient client(modem);

#include <PubSubClient.h>
WiFiClient espClient;
PubSubClient mqtt_client(MQTT_HOST, MQTT_PORT, espClient);

#if OTA_ENABLED
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#endif 

SenMLPack device(DEVICE_URN);
SenMLFloatRecord latitudeSenML(THINGSML_LATITUDE);
SenMLFloatRecord longitudeSenML(THINGSML_LONGITUDE);
SenMLFloatRecord batteryVoltageSenML(THINGSML_BATTERY_VOLTAGE);
// SenMLFloatRecord batteryLevel(THINGSML_BATTERY_LEVEL);
SenMLFloatRecord batteryLevelSenML("battery_level", SENML_UNIT_PERCENTAGE_REMAINING_BATTERY_LEVEL);
SenMLFloatRecord solarVoltageSenML("solar_voltage", SENML_UNIT_VOLT);
SenMLFloatRecord speedSenML("speed", SENML_UNIT_VELOCITY);
SenMLFloatRecord accuracySenML("accuracy", SENML_UNIT_NONE); // not sure what units these are in


float lat, lon, 
  speed,  // Speed Over Ground. Unit is knots // 1 knots == 1.85 km/ph
  alt,    // MSL Altitude. Unit is meters
  acc;    // HDOP - Horizontal Dilution Of Precision
int wifiNetworks = 0;
unsigned long lastScanMillis = 0;
bool sdMounted = false;
File logFile;

bool network_available(const char * name) {
  if (wifiNetworks == 0 || millis() - lastScanMillis >= 10*1000UL) {
    lastScanMillis = millis();
    wifiNetworks = WiFi.scanNetworks();
  }

  for (int i = 0; i < wifiNetworks; i++) {
    if(String(name) == WiFi.SSID(i)) {
      return true;
    }
  }
  return false;
}


void enableWifi() {
  WiFi.mode(WIFI_STA);
}

void disableWifi() {
  WiFi.mode(WIFI_OFF);
}

bool wifiConnected() {
  return WiFi.status() == WL_CONNECTED;
}

bool connectToWifi() {
  enableWifi();
  SerialMon.println("Checking WiFi");  
  for (size_t i = 0; i < (sizeof(known_networks) / sizeof(known_networks[0])); i++) {
    if(network_available(known_networks[i][0])){
      SerialMon.print("Found network, connecting to ");
      SerialMon.print(known_networks[i][0]);
      delay(10);
      WiFi.begin(known_networks[i][0], known_networks[i][1]);
      for (short int i = 0; i < 30; i++) {
        if(WiFi.status() != WL_CONNECTED) {
          delay(500);
          SerialMon.print(".");
        } else {
          SerialMon.println("connected!");
          SerialMon.print("IP address: ");
          SerialMon.print(WiFi.localIP());
          break;
        }
      }
      SerialMon.println("");
      return true;
    }
  }
  SerialMon.println("Network not found, turning off WiFi");
  disableWifi();
  return false;
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float readAdcVoltage(int pin) {
  uint32_t in = 0;
  for (int i = 0; i < ADC_SAMPLES; i++) {
    in += (uint32_t)analogRead(pin);
  }
  in = (int)in / ADC_SAMPLES;
  // based on 
  // https://github.com/Xinyuan-LilyGO/LilyGO-T-SIM7000G/blob/master/examples/Platformio_Arduino_Thingsboard/src/main.cpp#L376
  return ((float)in / 4096) * 3.6 * 2;
}

float getBatteryVoltage() {
  adc_power_acquire();
  pinMode(BAT_ADC_PIN, INPUT);
  float voltage = readAdcVoltage(BAT_ADC_PIN);
  adc_power_release();
  return voltage;
}

float getBatteryPercentage() {
  float voltage = getBatteryVoltage();
  if(voltage < 3.6)
    return 0.0;
  return mapfloat(voltage, 3.6, 4.25, 0, 100);
}

float getSolarVoltage() {
  adc_power_acquire();
  pinMode(BAT_ADC_PIN, INPUT);
  float voltage = readAdcVoltage(SOL_ADC_PIN);
  adc_power_release();
  return voltage;
}

void enableGPS(void) {
    // Set SIM7000G GPIO4 LOW ,turn on GPS power
    // CMD:AT+SGPIO=0,4,1,1
    // Only in version 20200415 is there a function to control GPS power
    modem.sendAT("+SGPIO=0,4,1,1");
    if (modem.waitResponse(10000L) != 1) {
        SerialMon.println(" SGPIO=0,4,1,1 false ");
    }
    modem.enableGPS();
}

void disableGPS(void) {
    // Set SIM7000G GPIO4 LOW ,turn off GPS power
    // CMD:AT+SGPIO=0,4,1,0
    // Only in version 20200415 is there a function to control GPS power
    modem.sendAT("+SGPIO=0,4,1,0");
    if (modem.waitResponse(10000L) != 1) {
        SerialMon.println(" SGPIO=0,4,1,0 false ");
    }
    modem.disableGPS();
}

void modemPowerOn() {
    pinMode(PWR_PIN, OUTPUT);
    digitalWrite(PWR_PIN, LOW);
    delay(1000);
    digitalWrite(PWR_PIN, HIGH);
}

void modemPowerOff() {
    pinMode(PWR_PIN, OUTPUT);
    digitalWrite(PWR_PIN, LOW);
    delay(1500); 
    digitalWrite(PWR_PIN, HIGH);
}

void modemRestart() {
    modemPowerOff();
    delay(1000);
    modemPowerOn();
}

bool ledState = false;

void toggleLed() {
  ledState = !ledState;
  digitalWrite(LED_PIN, ledState);
}

void ledOn() {
  ledState = true;
  digitalWrite(LED_PIN, HIGH);
}

void ledOff() {
  ledState = false;
  digitalWrite(LED_PIN, LOW);
}

void setup() {
  SerialMon.begin(115200);
  delay(100);

  pinMode(LED_PIN, OUTPUT);
  ledOff();

#if LOG_TO_SD
  SPI.begin(SD_SCLK, SD_MISO, SD_MOSI, SD_CS);
  if (!SD.begin(SD_CS)) {
    SerialMon.println("SDCard MOUNT FAIL");
  } else {
    sdMounted = true;
    uint32_t cardSize = SD.cardSize() / (1024 * 1024);
    String str = "SDCard Size: " + String(cardSize) + "MB";
    SerialMon.println(str);
    logFile = SD.open(LOG_FILE, FILE_APPEND);
    if (logFile) {
      logFile.println("-------- BOOT --------");
    } else {
      sdMounted = false; // just put it back to off
    }
  }
#endif

  SerialMon.println("Initializing modem...");
  if(sdMounted) logFile.println("Initializing modem...");
  modemRestart();
  SerialAT.begin(UART_BAUD, SERIAL_8N1, PIN_RX, PIN_TX);
  delay(100);
  if (!modem.testAT()) {
    SerialMon.println("Failed to restart modem, attempting to continue without restarting");
    if(sdMounted) logFile.println("Initializing modem...");
    modemRestart();
  }

  SerialMon.print("Modem Info: ");
  String modemInfo = modem.getModemInfo();
  SerialMon.println(modemInfo);
  if(sdMounted) {
    logFile.print("Modem Info: ");
    logFile.println(modemInfo);
  }

  if (GSM_PIN && modem.getSimStatus() != 3) { modem.simUnlock(GSM_PIN); }

  SerialMon.print("Waiting for network...");
  if (!modem.waitForNetwork()) {
    SerialMon.println(" failed");
    delay(10000);
  } else {
    SerialMon.println(" connected");
  }

  // GPRS connection parameters are usually set after network registration
  SerialMon.print("Connecting to ");
  SerialMon.print(GPRS_APN);
  if(sdMounted) {
    logFile.print("Connecting to ");
    logFile.print(GPRS_APN);
  }
  if (!modem.gprsConnect(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD)) {
    SerialMon.println(" failed");
    if(sdMounted) logFile.println(" failed");
    delay(10000);
  } else {
    SerialMon.println(" connected");
    if(sdMounted) logFile.println(" connected");
  }

  // SerialMon.println("Adding lat/long to SenMLPack");
  device.add(latitudeSenML);
  device.add(longitudeSenML);
  device.add(batteryVoltageSenML);
  device.add(batteryLevelSenML);
  device.add(solarVoltageSenML);
  device.add(speedSenML);
  device.add(accuracySenML);

  // powersaving
  // adc_power_off(); // deprecated
  // adc_power_release();
  btStop();

#if OTA_ENABLED 
  ArduinoOTA.setPort(OTA_PORT);
#ifdef OTA_HOST
  ArduinoOTA.setHostname(OTA_HOST);
#endif

#ifdef OTA_PASS
  ArduinoOTA.setPassword(OTA_PASS);
#endif

#ifdef OTA_HASH
  ArduinoOTA.setPasswordHash(OTA_HASH);
#endif

  ArduinoOTA.setMdnsEnabled(OTA_mDNS);

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
#endif
  if(sdMounted) {
    logFile.println("-------- BOOT --------");
    logFile.close();
  }
}

bool sendDataViaWifi() {
#if defined MQTT_USER && defined MQTT_PASS
  if (mqtt_client.connect(MQTT_ID, MQTT_USER, MQTT_PASS)) {
#else
  if (mqtt_client.connect(MQTT_ID)) {
#endif
    char buff[200] = {0};
    sprintf(
      buff, 
      "{"
      "\"latitude\": %f, \"longitude\": %f, \"gps_accuracy\": %f, \"speed\": %f,"
      "\"battery_level\": %.2f, \"battery_voltage\": %.2f, \"solar_voltage\": %.2f"
      "}",
      lat, 
      lon, 
      acc, 
      speed, 
      getBatteryPercentage(),
      getBatteryVoltage(),
      getSolarVoltage()
    );
    return mqtt_client.publish(MQTT_TOPIC, buff);
  }
  return false;
}

bool sendDataViaKPN() {
  SerialMon.println("Sending to KPN");
  char buff[800] = {0};

  if (!client.connect(HTTP_IP, HTTP_PORT)) {
      SerialMon.print("Connection failed: unable to connect to "); SerialMon.println(HTTP_IP);
      delay(500);
      return false;
  }

  latitudeSenML.set(lat); 
  longitudeSenML.set(lon); 
  batteryVoltageSenML.set(getBatteryVoltage());
  batteryLevelSenML.set(getBatteryPercentage());
  solarVoltageSenML.set(getSolarVoltage());
  speedSenML.set(speed);
  accuracySenML.set(acc);

  if(LOG_TO_SD && sdMounted) {
    logFile = SD.open(LOG_FILE, FILE_APPEND);
    logFile.println(buff); 
    logFile.close();
  }

  int len = ThingsML::httpPost(buff, 800, DEVICE_KEY, HTTP_HOST, HTTP_PATH, device);
  client.write((uint8_t *) buff, len);
  delay(4000);

  // while(client.available()) {
  //   SerialMon.print((char)client.read());
  // }
  // SerialMon.println();

  client.stop();
  return true;
}

bool getGPSPosition() {
  ledOn();
  enableGPS();
  bool found = false;
  int vsat, usat;
  for (size_t i = 0; i < 30; i++) {
      if (modem.getGPS(&lat, &lon, &speed, &alt, &vsat, &usat, &acc)) {
          // if(speed > 0)
          //   speed = speed * 1.85; // convert nautical knots to km/ph
          
          SerialMon.print("lat/long/acc: "); 
          SerialMon.print(lat, 8); SerialMon.print(","); SerialMon.print(lon, 8); SerialMon.print(" "); SerialMon.print(alt, 2);
          SerialMon.print(" "); SerialMon.println(acc, 2);
          if(LOG_TO_SD && sdMounted) {
            logFile = SD.open(LOG_FILE, FILE_APPEND);
            logFile.print("lat/long/acc: "); 
            logFile.print(lat, 8); logFile.print(","); logFile.print(lon, 8); logFile.print(" "); logFile.print(alt, 2);
            logFile.print(" "); logFile.println(acc, 2);
            logFile.close();
          }
          found = true;
          break;
      }
      delay(500);
  }
  disableGPS();
  ledOff();
  return found;
}

bool _loopGSM() {
  if (!modem.isNetworkConnected()) {
    SerialMon.println("Network disconnected");
    if (!modem.waitForNetwork(180000L, true)) {
      SerialMon.println(" fail");
      delay(5000);
      return false;
    } else {
      SerialMon.println("Network re-connected");
    }

    // and make sure GPRS/EPS is still connected
    if (!modem.isGprsConnected()) {
      SerialMon.println("GPRS disconnected!");
      SerialMon.print("Connecting to ");
      SerialMon.print(GPRS_APN);
      if (!modem.gprsConnect(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD)) {
        SerialMon.println(" fail");
        delay(5000);
        return false;
      } else {
        SerialMon.println(" reconnected");
      }
    }
  }

  if(getGPSPosition()) {
    return sendDataViaKPN();
  } else {
    SerialMon.println("Could not get GPS");
  }
  return false;
}


unsigned long lastGpsMillis = 0;
unsigned long lastWifiMillis = 0;

void loop() {
#ifdef NO_WIFI_LOGGING
#if OTA_ENABLED
  if(wifiConnected()) {
    ArduinoOTA.handle();
  }
#endif
#else
  if(wifiConnected()) {
#if OTA_ENABLED
    ArduinoOTA.handle();
#endif
    mqtt_client.loop();
    if (millis() - lastGpsMillis >= intervalGpsViaWifiMillis) {
      if(getGPSPosition()) { 
        if(sendDataViaWifi()) {
          lastGpsMillis = millis();
        } else {
          SerialMon.println("Could not report GPS via WiFi");
        }
      } else {
        SerialMon.println("Could not get GPS");
      }
    }
  } else
#endif 
  
  if (millis() - lastGpsMillis >= intervalGpsViaGsmMillis) {
    if(_loopGSM()) {
      lastGpsMillis = millis();
    } else {
      // if failed wait an additional 100ms
      delay(100);
    }
  }

  if (!wifiConnected() && (lastWifiMillis == 0 || millis() - lastWifiMillis >= intervalWifiMillis)) {
#if OTA_ENABLED
    if(connectToWifi()) {
      ArduinoOTA.begin();
    }
#else
    connectToWifi();
#endif
    lastWifiMillis = millis();
  }

  // esp_sleep_enable_timer_wakeup(1 * uS_TO_S_FACTOR);
  // esp_light_sleep_start();
  delay(50);
}
